import React from 'react';
import { Link } from 'react-router-dom';

function HatColumns(props) {
    return (
        <div className="col">
            {props.list.map(data => {
                const hat = data;
                return (
                    <div key={hat.id} className="card mb-3 shadow">
                        <img src={hat.picture_url} className="card-img-top" />
                        <div className="card-body">
                            <h3 className="card-title">{hat.style_name}</h3>
                            <h4 className="card-subtitle mb-2 text-muted">
                                {hat.fabric}
                            </h4>
                            <h4 className="card-subtitle mb-2 text-muted">
                                {hat.color}
                            </h4>
                            <h4 className="card-subtitle mb-2 text-muted">
                                {hat.location.closet_name} - {hat.location.section_number} - {hat.location.shelf_number}
                            </h4>
                            <button className="btn btn-primary" onClick={() => props.delete(hat.id)}>Delete</button>
                        </div>
                    </div>
                );
            })}
        </div>
    );
}

class HatList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hatColumns: [[], [], []],
        };
        this.handleDelete = this.handleDelete.bind(this);
    }

    async handleDelete(id) {
        const hatUrl = `http://localhost:8090/api/hats/${id}/`;

        const fetchConfig = {
            method: "delete",
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            this.componentDidMount();
        }
    }



    async componentDidMount() {
        const url = 'http://localhost:8090/api/hats/';

        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();

                const requests = [];
                for (let hat of data.hats) {
                    const detailUrl = `http://localhost:8090/api/hats/${hat.id}/`;
                    requests.push(fetch(detailUrl));
                }

                const responses = await Promise.all(requests);

                const hatColumns = [[], [], []];


                let i = 0;
                for (const hatResponse of responses) {
                    if (hatResponse.ok) {
                        const details = await hatResponse.json();
                        hatColumns[i].push(details);
                        i = i + 1;
                        if (i > 2) {
                            i = 0;
                        }
                    } else {
                        console.error(hatResponse);
                    }
                }

                this.setState({ hatColumns: hatColumns });
            }
        } catch (e) {
            console.error(e);
        }
    }

    render() {
        return (
            <>
                <div className="container">
                    <h2>All the hats!</h2>
                    <div className="row">
                        {this.state.hatColumns.map((hatList, index) => {
                            return (
                                <HatColumns key={index} list={hatList} delete={this.handleDelete} />
                            );
                        })}
                    </div>
                </div>
            </>
        );
    }
}

export default HatList;
