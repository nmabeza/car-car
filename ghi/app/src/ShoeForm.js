import { useEffect, useState } from "react";

const ShoeForm = () => {
	const [manufacturer, setManufacturer] = useState("");
	const [modelName, setModelName] = useState("");
	const [color, setColor] = useState("");
	const [pictureUrl, setPictureUrl] = useState("");
	const [bins, setBins] = useState([]);
	const [selectedBin, setSelectedBin] = useState("");
	const [submitted, setSubmitted] = useState(false);
	// const [loaded, setLoaded] = useState(false);

	useEffect(() => {
		const fetchBins = async () => {
			const url = "http://localhost:8100/api/bins/";
			const response = await fetch(url);

			if (response.ok) {
				const data = await response.json();
				console.log(data);
				setBins(data.bins);
			}
		};
		fetchBins();
		// setLoaded(true);
	}, []);

	const handleSubmit = async (event) => {
		event.preventDefault();
		const data = { manufacturer, modelName, color, pictureUrl, selectedBin };
		data.model_name = data.modelName;
		data.picture_url = data.pictureUrl;
		data.bin = data.selectedBin;

		delete data.modelName;
		delete data.pictureUrl;
		delete data.selectedBin;
		console.log(data);

		const shoesUrl = "http://localhost:8080/api/shoes/";
		const fetchConfig = {
			method: "post",
			body: JSON.stringify(data),
			headers: {
				"Content-Type": "application/json",
			},
		};

		const response = await fetch(shoesUrl, fetchConfig);
		if (response.ok) {
			const newShoe = await response.json();
			console.log(newShoe);
			event.target.reset();
			setManufacturer("");
			setModelName("");
			setColor("");
			setPictureUrl("");
			setSelectedBin("");
			setSubmitted(true);
		}
	};

	return (
		<div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4">
					<h1>Create a New Shoe</h1>
					<form id="create-presentation-form" onSubmit={handleSubmit}>
						<div className="form-floating mb-3">
							<input
								onChange={(e) => setManufacturer(e.target.value)}
								placeholder="Manufacturer"
								required
								type="text"
								name="manufacturer"
								id="manufacturer"
								className="form-control"
							/>
							<label htmlFor="manufacturer">Manufacturer</label>
						</div>
						<div className="form-floating mb-3">
							<input
								onChange={(e) => setModelName(e.target.value)}
								placeholder="modelName"
								required
								type="modelName"
								name="modelName"
								id="modelName"
								className="form-control"
							/>
							<label htmlFor="modelName">Model Name</label>
						</div>
						<div className="form-floating mb-3">
							<input
								onChange={(e) => setColor(e.target.value)}
								placeholder="color"
								type="text"
								name="color"
								id="color"
								className="form-control"
							/>
							<label htmlFor="color">Color</label>
						</div>
						<div className="form-floating mb-3">
							<input
								onChange={(e) => setPictureUrl(e.target.value)}
								placeholder="pictureUrl"
								required
								type="text"
								name="pictureUrl"
								id="pictureUrl"
								className="form-control"
							/>
							<label htmlFor="pictureUrl">Picture Url</label>
						</div>
						<div className="mb-3">
							<select
								onChange={(e) => setSelectedBin(e.target.value)}
								required
								name="bin"
								id="bin"
								className="form-select">
								<option value="">Select a Bin</option>
								{bins.map((bin) => {
									return (
										<option key={bin.id} value={bin.href}>
											{bin.closet_name}
										</option>
									);
								})}
							</select>
						</div>
						<button className="btn btn-primary">Create</button>
					</form>
				</div>
			</div>
		</div>
	);
};
export default ShoeForm;
